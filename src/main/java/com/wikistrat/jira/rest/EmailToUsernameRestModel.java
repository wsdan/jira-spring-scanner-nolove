package com.wikistrat.jira.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class EmailToUsernameRestModel {

    @XmlElement(name = "value")
    private String message;

    public EmailToUsernameRestModel() {
    }

    public EmailToUsernameRestModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}