package com.wikistrat.jira.rest;

import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.ApplicationUser;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.lang.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * A resource of message.
 */
@Scanned
@Path("/message")
public class EmailToUsernameRest {

    private final UserSearchService userSearchService;

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static final String TEST_EMAIL = "admin@example.com";

    @Autowired
	public EmailToUsernameRest(@ComponentImport final UserSearchService userSearchService) {
        this.userSearchService = userSearchService;
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessage()
    {
        final Iterable<ApplicationUser> usersByEmail = userSearchService.findUsersByEmail(TEST_EMAIL);

        for(ApplicationUser user : usersByEmail){
            return Response.ok(new EmailToUsernameRestModel( user.getUsername() )).build();
        }

        return Response.ok(new EmailToUsernameRestModel( "Failed, Whale" )).build();
    }

}